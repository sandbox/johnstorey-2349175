<?php
/**
 * @file
 * interfax_fax_record.features.inc
 */

/**
 * Implements hook_node_info().
 */
function interfax_fax_record_node_info() {
  $items = array(
    'interfax_record' => array(
      'name' => t('Interfax Record'),
      'base' => 'node_content',
      'description' => t('Records faxes sent and updates status through callbacks from Interfax.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
